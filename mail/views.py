from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from .models import Email
from .serializers import EmailListSerializer, SendEmailSerializer

class EmailList(ListAPIView):
    serializer_class = EmailListSerializer
    permission_classes = [AllowAny,]

    def get_queryset(self):
        username = self.request.query_params.get("username", None)
        if username:
            return Email.objects.filter(receiver__username=username).order_by('-id')
        raise NotFound()

class SendEmail(CreateAPIView):
    serializer_class = SendEmailSerializer
    permission_classes = [AllowAny,]
