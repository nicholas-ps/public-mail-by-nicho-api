from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Email(models.Model):
    sender = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=False,
        related_name='sender'
    )
    receiver = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=False,
        related_name='receiver'
    )
    subject = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    on_created = models.TimeField(auto_now_add=True)
