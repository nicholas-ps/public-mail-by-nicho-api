from django.urls import path
from .views import EmailList, SendEmail

urlpatterns = [
    path('inbox', EmailList.as_view()),
    path('send', SendEmail.as_view()),
]
