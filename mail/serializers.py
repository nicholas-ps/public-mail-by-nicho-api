from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Email

DOMAIN = "publicmailbynicho.com"

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'username',
            'email'
        )

class EmailListSerializer(serializers.ModelSerializer):
    sender = UserSerializer()
    receiver = UserSerializer()

    class Meta:
        model = Email
        fields = '__all__'

class SendEmailSerializer(serializers.ModelSerializer):
    sender = serializers.CharField()
    receiver = serializers.CharField()

    class Meta:
        model = Email
        fields = '__all__'
    
    def create(self, validated_data):
        sender_username = validated_data["sender"]
        sender_email = sender_username + "@" + DOMAIN

        receiver_username = validated_data["receiver"]
        receiver_email = receiver_username + "@" + DOMAIN

        if User.objects.filter(username=sender_username).exists():
            sender = User.objects.get(username=sender_username)
        else:
            sender = User.objects.create(
                username=sender_username,
                email=sender_email
            )

        if User.objects.filter(username=receiver_username).exists():
            receiver = User.objects.get(username=receiver_username)
        else:
            receiver = User.objects.create(
                username=receiver_username,
                email=receiver_email
            )

        return Email.objects.create(
            sender=sender,
            receiver=receiver,
            subject=validated_data["subject"],
            content=validated_data["content"]
        )
